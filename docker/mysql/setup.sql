create database test_db;
Use test_db;

Create table `tbl_fleet_ships`
(
    ID     int          not NULL AUTO_INCREMENT,
    Name   varchar(255) not NULL,
    Class  varchar(255) not NULL,
    Crew   int,
    Image  varchar(255),
    Value  double,
    Status varchar(255) not NULL,
    PRIMARY KEY (ID)
);

Create table `tbl_ship_armaments`
(
    ID           int          not NULL AUTO_INCREMENT,
    ShipId       varchar(255) not NULL,
    ArmamentName varchar(255) not NULL,
    Quantity     varchar(255) not NULL,
    PRIMARY KEY (id)
);

Create table `tbl_users`
(
    username varchar(255) not NULL,
    password varchar(255) not NULL
);

insert into tbl_users (username, password)
values ('test', 'test');

insert into tbl_fleet_ships (ID, Name, Class, Crew, Image, Value, Status)
values (1, 'Devastator1', 'StarDestroyer', 35000, 'https:\\url.to.image', 1999.99, 'operational');
insert into tbl_fleet_ships (ID, Name, Class, Crew, Image, Value, Status)
values (2, 'Devastator2', 'StarDestroyer', 35000, 'https:\\url.to.image', 1999.99, 'damaged');
insert into tbl_fleet_ships (ID, Name, Class, Crew, Image, Value, Status)
values (3, 'Devastator3', 'BigShip', 35000, 'https:\\url.to.image', 1999.99, 'destryoed');
insert into tbl_ship_armaments (ID, ShipId, ArmamentName, Quantity)
values (1, 1, 'Turbo Laser', 60);
insert into tbl_ship_armaments (ID, ShipId, ArmamentName, Quantity)
values (2, 1, 'Ion Cannons', 60);
insert into tbl_ship_armaments (ID, ShipId, ArmamentName, Quantity)
values (3, 1, 'Tractor Beam', 10);

