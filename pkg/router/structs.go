package router

import "net/http"

type Route struct {
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}
type RouteGroup struct {
	defaultRoutes []Route
	authRoutes    []Route
}
