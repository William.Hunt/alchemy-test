package logic

import (
	"AlchemyTest/internal/structs"
	"AlchemyTest/pkg/database"
	"errors"
)

func GetAllShips() (ships []map[string]interface{}) {
	return database.GetQueryAsArray(`select ID,Name,Status from tbl_fleet_ships`, database.MySql)
} /*func GetAllShips() (ships []*structs.Ship) {
	shipsArray := database.GetQueryAsArray(`select * from tbl_fleet_ships`, database.MySql)
	for _, ship := range shipsArray {
		ships = append(ships, arrayToShip(ship))
	}
	return
}*/
func GetFilteredShips(field string, value string) (ships []map[string]interface{}) {
	return database.GetQueryAsArray(`select ID,Name,Status from tbl_fleet_ships where ? = ?`, database.MySql, field, value)
}

//func GetFilteredShips(field string, value string) (ships []*structs.Ship) {
//	shipsArray := database.GetQueryAsArray(`select * from tbl_fleet_ships where ? = ?`, database.MySql, field, value)
//	for _, ship := range shipsArray {
//		ships = append(ships, arrayToShip(ship))
//	}
//	return
//}
func GetShip(id string) (ship *structs.Ship) {
	shipsArray := database.GetQueryAsArray(`select * from tbl_fleet_ships where id = ?`, database.MySql, id)
	if len(shipsArray) == 1 {
		ship = arrayToShip(shipsArray[0])
	} else {
		panic(errors.New("Ship ID not recognised"))
	}
	ship.Aramanents = getArmaments(ship.ID)
	return
}
func CreateShip(postData map[string]interface{}) {}
func UpdateShip()                                {}
func DeleteShip()                                {}

func arrayToShip(shipMap map[string]interface{}) (ship *structs.Ship) {
	ship = &structs.Ship{}
	ship.ID = shipMap["ID"].(int64)
	ship.Name = shipMap["Name"].(string)
	ship.Class = shipMap["Class"].(string)
	ship.Crew = shipMap["Crew"].(int64)
	ship.Image = shipMap["Image"].(string)
	ship.Value = shipMap["Value"].(float64)
	ship.Status = shipMap["Status"].(string)
	return
}

func getArmaments(shipID int64) (armaments []*structs.Armament) {
	armamentsArray := database.GetQueryAsArray(`select * from tbl_ship_armaments where ShipId = ?`, database.MySql, shipID)
	for _, armament := range armamentsArray {
		armaments = append(armaments, arrayToArmament(armament))
	}
	return
}
func arrayToArmament(armamentMap map[string]interface{}) (armament *structs.Armament) {
	armament = &structs.Armament{}
	armament.ID = armamentMap["ID"].(int64)
	armament.Name = armamentMap["ArmamentName"].(string)
	armament.Quantity = armamentMap["Quantity"].(string)
	return
}
