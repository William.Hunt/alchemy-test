package structs

type Ship struct {
	ID         int64
	Name       string
	Class      string
	Crew       int64
	Image      string
	Value      float64
	Status     string
	Aramanents []*Armament
}
type Armament struct {
	ID       int64
	Name     string
	Quantity string
}
