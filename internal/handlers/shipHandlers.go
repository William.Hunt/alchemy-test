package handlers

import (
	"AlchemyTest/internal/logic"
	"AlchemyTest/pkg/database"
	"AlchemyTest/pkg/router"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
)

func init() {
	router.AddDef(router.Route{Method: "GET", Pattern: "/Ship", HandlerFunc: getAllShips})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Ship/name/{value}", HandlerFunc: getAllShipsName})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Ship/status/{value}", HandlerFunc: getAllShipsStatus})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Ship/type/{value}", HandlerFunc: getAllShipsType})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Ship/{id}", HandlerFunc: getShip})
	router.AddAuth(router.Route{Method: "POST", Pattern: "/Ship", HandlerFunc: createShip})
	router.AddAuth(router.Route{Method: "PUT", Pattern: "/Ship", HandlerFunc: updateShip})
	router.AddAuth(router.Route{Method: "DELETE", Pattern: "/Ship/{id}", HandlerFunc: deleteShip})
}

func getAllShips(w http.ResponseWriter, r *http.Request) {
	jsonString, err := json.Marshal(logic.GetAllShips())
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func getAllShipsName(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	jsonString, err := json.Marshal(logic.GetFilteredShips("Name", data["value"]))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func getAllShipsStatus(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	jsonString, err := json.Marshal(logic.GetFilteredShips("Status", data["value"]))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func getAllShipsType(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	jsonString, err := json.Marshal(logic.GetFilteredShips("Type", data["value"]))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}

func getShip(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	shipData := logic.GetShip(data["id"])
	jsonString, err := json.Marshal(shipData)
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))

}
func createShip(w http.ResponseWriter, r *http.Request) {
	tx, _, postData, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	sql := `insert into tbl_fleet_ships (Name, Class, Crew, Image, Value, Status)
values (?,?,?,?,?,?)`
	params = append(params, postData["Name"], postData["Class"], postData["Crew"], postData["Image"], postData["Value"], postData["Status"])
	database.RunDataChange(sql, tx, params...)
	v := reflect.ValueOf(postData["Armaments"])
	if v.Kind() == reflect.Map {
		v := reflect.ValueOf(postData["Armaments"])
		if v.Kind() == reflect.Map {
			for _, key := range v.MapKeys() {
				strct := v.MapIndex(key)
				sql := `insert into tbl_ship_armaments (Name, Class, Crew, Image, Value, Status) values (?,?,?,?,?,?)`
				params = append(params, strct, postData["Class"], postData["Crew"], postData["Image"], postData["Value"], postData["Status"])
				database.RunDataChange(sql, tx, params...)
			}
		}
	}

	tx.Commit()
	fmt.Fprintln(w, "successfully created account")
}
func updateShip(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	tx, _, postData, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	sql := `update tbl_fleet_ships set Name=?, Class=?, Crew=?, Image=?, Value=?, Status=? where id = ?`
	params = append(params, postData["Name"], postData["Class"], postData["Crew"], postData["Image"], postData["Value"], postData["Status"], data["id"])
	database.RunDataChange(sql, tx, params...)
	v := reflect.ValueOf(postData["Armaments"])
	if v.Kind() == reflect.Map {
		v := reflect.ValueOf(postData["Armaments"])
		if v.Kind() == reflect.Map {
			for _, key := range v.MapKeys() {
				strct := v.MapIndex(key)
				sql := `insert into tbl_ship_armaments (Name, Class, Crew, Image, Value, Status) values (?,?,?,?,?,?)`
				params = append(params, strct, postData["Class"], postData["Crew"], postData["Image"], postData["Value"], postData["Status"])
				database.RunDataChange(sql, tx, params...)
			}
		}
	}

	tx.Commit()
	fmt.Fprintln(w, "successfully created account")
}
func deleteShip(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	tx, _, _, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	sql := `delete from tbl_fleet_ships where id = ?`
	params = append(params, data["id"])
	database.RunDataChange(sql, tx, params...)
	tx.Commit()
	fmt.Fprintln(w, "successfully created account")
}
