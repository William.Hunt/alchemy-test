package handlers

import (
	"AlchemyTest/pkg/database"
	"AlchemyTest/pkg/router"
	"encoding/json"
	"net/http"
)

type loginStruct struct {
	Username string
	Password string
}

func init() {
	router.AddDef(router.Route{Method: "POST", Pattern: "/login", HandlerFunc: Login})
	router.AddAuth(router.Route{Method: "GET", Pattern: "/tknrefresh", HandlerFunc: RefreshToken})
}

//this route refreshes the json web token to keep the user logged in between page refreshes
func RefreshToken(w http.ResponseWriter, r *http.Request) {
	jwtData, _ := r.Context().Value(database.MyKey).(database.JwtData)
	token := router.SetToken(jwtData.Username, []byte(database.JsonKey))
	response, _ := json.Marshal(token)
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

//this route handles logins
func Login(w http.ResponseWriter, r *http.Request) {
	var loginParams loginStruct
	err := json.NewDecoder(r.Body).Decode(&loginParams)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	ok, username := Authenticate(loginParams.Username, loginParams.Password)
	if ok {
		token := router.SetToken(username, []byte(database.JsonKey))
		response, _ := json.Marshal(token)
		w.WriteHeader(http.StatusOK)
		w.Write(response)
	} else {
		http.Error(w, "Incorrect Username/password", http.StatusUnauthorized)
	}
}

func Authenticate(username string, password string) (bool, string) {
	if len(database.GetQueryAsArray(`select * from tbl_users where username = ? and password = ?`, database.MySql, username, password)) == 1 {
		return true, username
	} else {
		return false, ""
	}

}
